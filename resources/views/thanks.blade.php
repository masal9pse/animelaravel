@extends('layout')

{{-- このようにcontainerとは、コンテンツを真ん中に配置する為の大きな枠という役割を持っている。 --}}
@section('content')
<div class="container d-flex align-items-cebter main">
  <div class="border p-4">
    <h1 class="h5 mb-4 text-warning ">
      ありがとうございます。今後参考にさせて頂きます。
    </h1>
  </div>
  <div class="mt-5 ml-4">
    <a class="btn btn-secondary" href="{{ route('top') }}">
      トップページに戻る
    </a>
  </div>
</div>
@endsection

<style>
  .main {
    margin-top: 80px;
  }
</style>