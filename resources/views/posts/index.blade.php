@extends('layout')

@section('content')

<div class="container mt-4">
  <div class="mb-4">
    <a href="{{ route('posts.create') }}" class="btn btn-warning">
      リクエストしたいタイトルはありますか？
    </a>
  </div>
  @foreach ($posts as $post)
  <div class="card mb-4">
    <div class="card-header">
      <span>{{ $post->title }}</span>
      <span class="mr-2">
        投稿日時 {{ $post->created_at->format('Y.m.d') }}
      </span>
      　　　</div>
  </div>
  @endforeach
</div>
@endsection